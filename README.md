# Kubectl utilities

Simple bash script that installs

* kubectl
* kubectl krew plugin manager

and some additional [nice-to-have features](#features).

## Installation

```
curl https://codeberg.org/timoses/kubectl_utils/raw/branch/master/install.sh | bash
```

You can use the same command to update your installation.

## Features

'{xxx}' denotes the supported shells and conditions. Support for further shells may be implemented in the future.

### Aliases

The script additionally sets up aliases for quicker interaction with `kubectl`.

The alias `kube` will print a list of available aliases.

Some aliases require you to install other components:
* `kon`|`koff`: [kube-ps1](https://github.com/jonmosco/kube-ps1) {zsh w/ antibody}
* `kctx`|`kns`: [kubectx](https://github.com/ahmetb/kubectx) (includes `kns`) {zsh w/ antibody}
* `ktail`: [kubetail](https://github.com/johanhaleby/kubetail) {zsh w/ antibody}


## Contributing

If you feel like anything is missing (e.g. support for other shells or some kubectl related feature) feel free to create a PR.
